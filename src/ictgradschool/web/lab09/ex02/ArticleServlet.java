package ictgradschool.web.lab09.ex02;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class ArticleServlet extends HttpServlet {

//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//        // request.getParameter accesses parameters from the submitted form
//        String title = request.getParameter("title");
//        String author = request.getParameter("author");
//        String genre = request.getParameter("type");
//        String content = request.getParameter("content");
//
//        // printWriter is used to write to the HTML document
//        PrintWriter out = response.getWriter();
//        response.setContentType("text/html");
//
//        out.println("<b>"+title+"</b>");
//        out.println("by " + author + "<br><br>");
//        out.println("Genre: " + genre +"<br><br>");
//        out.println(content);
//
//
//
//    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // request.getParameter accesses parameters from the submitted form
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String genre = request.getParameter("genre");
        String content = request.getParameter("content");

        // printWriter is used to write to the HTML document
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

//        out.println("<html>");
//        out.println("<head><meta charset=\"UTF-8\"><title>ProfileServlet</title>\n");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>"+title+"</h1>");
        out.println("by " + author + "<br><br>");
        out.println("Genre: " + genre +"<br><br>");
        out.println(content+"<br><br>");
        out.println("<h2>This is a String array from Java:</h2>"+"<br><br>");

        //for loop
        String[] myArr = new String[]{"four","three","two","one"};
        out.println("<ul>");
        for(int i=0;i<myArr.length;i++){
            out.println("<li>"+myArr[i]+"</li>" );
        }
        out.println("</ul>");
//        out.println("</body></html>");
    }
}
