package ictgradschool.web.lab09.ex07;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageGalleryDisplay extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        PrintWriter out = response.getWriter();
        out.println("<html>\n<head><meta charset=\"UTF-8\"><title>Server response</title>");
        out.println("</head>\n<body>");
        out.println("<h1>Server side response</h1>");

        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("Photos");
//        out.println(fullPhotoPath);
        File folder = new File(fullPhotoPath);
        File[] listOfImages = folder.listFiles();
        out.println("<div>");


        for (File file : listOfImages) {
//            out.println(file.getName());
            if (file.getName().contains("thumbnail")) {
                String bigImage = file.getName().substring(0, file.getName().indexOf("_thumbnail")) + ".jpg";
//                out.println(bigImage);
//                out.println(fullPhotoPath + "\\" + file.getName());
                out.println("<h3>" + "Thumnail:" + file.getName() + ", size: " + file.length() + " bytes" + "</h3>");
//                out.println("<img src=\"" + "/web_lab_09/Photos/" + file.getName() + "\">");
                out.println("<a href=\"" + "/web_lab_09/Photos/" + "/" + bigImage + "\">" + "<img src=\"" + "/web_lab_09/Photos/" + file.getName() + "\">" + "</a>");
            }

        }
        out.println("</div>\n</body>\n</html>");
    }
}

