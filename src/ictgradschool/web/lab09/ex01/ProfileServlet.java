package ictgradschool.web.lab09.ex01;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ProfileServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // request.getParameter accesses parameters from the submitted form
        String fname = request.getParameter("fname");
        String Lname = request.getParameter("Lname");
        String biography = request.getParameter("comment");

        // printWriter is used to write to the HTML document
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");


        out.println("<html>");
        out.println("<head><meta charset=\"UTF-8\"><title>ProfileServlet</title>\n");
        out.println("</head>");
        out.println("<body>");
        out.println("First Name: " + fname + "<br>");
        out.println("Last Name: " + lname + "<br><br>");
        out.println("<b>Biography: </b>" + "<br>" + biography +"<br><br>");
        out.println("<h1>Biography</h1>");
        out.println("<p>"+bio+"</p>");
        out.println("</body></html>");





    }
}
